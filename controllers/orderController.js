const Order = require("../models/Order");
const Product = require("../models/Product");

module.exports.createOrder = async (reqBody, authData, err) => {
  if (err) {
    console.log(err);
  } else {
    let productData = await Product.findById(reqBody.productId);
    let newOrder = new Order({
      userId: authData.id,
      products: [
        {
          productId: reqBody.productId,
          quantity: reqBody.quantity,
        },
      ],
      totalAmount: productData.price * reqBody.quantity,
    });
    newOrder.save();
    return true;
  }
};

module.exports.retrieveUserOrders = async (authData, err) => {
  if (err) {
    console.log(err);
  } else {
    return Order.find({ userId: authData.id });
  }
};

module.exports.allOrders = async (authData, err) => {
  if (err) {
    console.log(err);
  } else if (authData.isAdmin) {
    return Order.find({});
  } else {
    let message = "Restricted page";
    return message;
  }
};

module.exports.addToCart = async (data, reqParams, err) => {
  if (err) {
    console.log(err);
  } else {
  }
};
