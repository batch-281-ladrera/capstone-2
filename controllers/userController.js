const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.registerUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    // User does not exist
    if (result === null) {
      let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 10),
      });
      newUser.save();
      return true;
      // User exists
    } else {
      return false;
    }
  });
};

// User authentication
module.exports.loginUser = (reqBody) => {
  // the findOne method returns the first record in the collection that matches the search criteria
  // We use findOne method instead of find method which returns all records that match the criteria
  return User.findOne({ email: reqBody.email }).then((result) => {
    // User does not exist
    if (result === null) {
      return false;

      // User exists
    } else {
      // Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
      // The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result

      const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

      // If the password matches
      if (isPasswordCorrect) {
        // Generate an access token
        // Uses the createAccessToken method defined in the auth.js file
        return { access: auth.createAccessToken(result) };

        // Passwords do not match
      } else {
        return false;
      }
    }
  });
};

module.exports.getProfile = (data) => {
  return User.findById(data.userId).then((result) => {
    // Changes the value of the user's password to an empty string when returned to the frontend
    // Not doing so will expose the user's password which will also not be needed in other parts of our application
    // Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
    result.password = "";

    // Returns the user information with the password as an empty string
    return result;
  });
};

module.exports.toAdmin = async (reqParams, authData, err) => {
  if (authData.isAdmin) {
    let partialData = await User.findByIdAndUpdate(reqParams.id, { isAdmin: true });
    let message = `User ${partialData.email} is now an admin`;
    return message;
  } else if (err) {
    console.log(err);
  } else {
    message = "User logged in is not an admin";
    return true;
  }
};
