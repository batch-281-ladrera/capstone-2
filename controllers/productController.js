const Product = require("../models/Product");

module.exports.createProduct = async (reqBody, authData, err) => {
  if (authData.isAdmin) {
    let newProduct = new Product({
      name: reqBody.name,
      description: reqBody.description,
      price: reqBody.price,
      imageUrl: reqBody.imageUrl,
    });
    await newProduct.save();
    let message = "Product created";
    return true;
  } else if (err) {
    console.log(err);
  } else {
    let message = "Restricted page";
    return false;
  }
};

module.exports.allProducts = async (err) => {
  if (err) {
    console.log(err);
  } else {
    return Product.find({});
  }
};

module.exports.allActive = async (err) => {
  if (err) {
    console.log(err);
  } else {
    return Product.find({ isActive: true });
  }
};

module.exports.specificProduct = async (reqParams, err) => {
  if (err) {
    console.log(err);
  } else {
    return Product.findOne({ _id: reqParams.id });
  }
};

module.exports.updateProduct = async (reqParams, reqBody, authData, err) => {
  if (authData.isAdmin) {
    let updatedProduct = {
      name: reqBody.name,
      description: reqBody.description,
      price: reqBody.price,
    };
    await Product.findByIdAndUpdate(reqParams.id, updatedProduct);
    let message = "Update Successful";
    return true;
  } else if (err) {
    console.log(err);
  } else {
    message = "Restricted page";
    return false;
  }
};

module.exports.archivedProduct = async (reqParams, authData, err) => {
  if (authData.isAdmin) {
    await Product.findByIdAndUpdate(reqParams.id, { isActive: false });
    let message = "Archive successful";
    return true;
  } else if (err) {
    return false;
  } else {
    message = "Restricted page";
    return false;
  }
};

module.exports.activateProduct = async (reqParams, authData, err) => {
  if (authData.isAdmin) {
    await Product.findByIdAndUpdate(reqParams.id, { isActive: true });
    let message = "Product Activated";
    return true;
  } else if (err) {
    return false;
  } else {
    message = "Restricted page";
    return false;
  }
};
